#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

#define N 4
#define T topRightCorner(2, 1) // Translation Matrix
#define R topLeftCorner(2, 2)  //Rotation Matrix

void icpKabsch(MatrixXd& previous, MatrixXd& current, Matrix3d& H)
{

  auto centroid_previous = previous.rowwise().mean();
  auto centroid_current = current.rowwise().mean();

  H.T = -centroid_previous;
  MatrixXd center_previous = previous + H.T.replicate(1, N);
  H.T = -centroid_current;
  MatrixXd center_current = current + H.T.replicate(1, N);

  MatrixXd cov = center_previous * center_current.transpose();

  JacobiSVD<MatrixXd> svd(cov, ComputeFullU | ComputeFullV);
  double d = (svd.matrixV() * svd.matrixU().transpose()).determinant();

  if (d > 0) d = 1.0;
  else d = -1.0;

  Matrix2d I = Matrix2d::Identity();
  I(1, 1) = d;

  H.R = svd.matrixV() * I *svd.matrixU().transpose();
  H.T = centroid_current  - (H.R * centroid_previous);

  previous = H.R * previous + H.T.replicate(1, N);
  cout << "H\n" << H << endl;
  cout <<"Previous:\n" << previous << endl;

  cout << "Final Angle:\n" << atan2(H(1,0), H(0,0)) << endl;
  cout << "Final Translation:\n" << H.T << endl;
}

int main()
{

    Matrix3d H = Matrix3d::Identity();

    MatrixXd previousMatrix(2, N);
    previousMatrix<< 1, 2, 2, 2, //x
                     1, 2, 4, 4; //y
    cout <<"Previous\n" << previousMatrix << endl;
    
    double theta = 0.0534; //5 deg
    H.R << cos(theta), -sin(theta),
             sin(theta),  cos(theta);
    H.T << 0.3, 0.2;

    MatrixXd currentMatrix(2, N);
    currentMatrix << H.R * previousMatrix + H.T.replicate(1,N);
    cout <<"Current\n" << currentMatrix << endl;

    //reset H
    H = Matrix3d::Identity();
    icpKabsch(previousMatrix, currentMatrix, H);

    return 0;
}

//References
//==========
//(1) - Kabsch, Wolfgang (1976). "A solution for the best rotation to relate two sets of vectors". Acta Crystallographica. A32 (5): 922. Bibcode:1976AcCrA..32..922K. doi:10.1107/S0567739476001873. 
